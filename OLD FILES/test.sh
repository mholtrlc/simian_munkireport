#!/bin/bash
CURL="/usr/bin/curl --insecure --fail --silent  --show-error"
BASEURL=http://report.reallifechurch.org/
MUNKIPATH=/usr/local/munki/
CACHEPATH=/usr/local/munki/preflight.d/cache/

MODULE_NAME="ard"
MODULESCRIPT="init_ard"
PREF_FILE="/Library/Preferences/com.apple.RemoteDesktop.plist"

CTL="${BASEURL}index.php?/module/${MODULE_NAME}/"

# Get the scripts in the proper directories

${CURL} "${CTL}get_script/${MODULESCRIPT}" -o "./${MODULESCRIPT}"