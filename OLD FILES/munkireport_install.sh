#!/bin/bash
#CHANGE THESE VARIABLES FOR YOUR ENVIRONMENT
BASEURL="http://report.reallifechurch.org/"
MUNKIPATH="/usr/local/munki/"
CACHEPATH="${MUNKIPATH}preflight.d/cache/"
PREFPATH="/Library/Preferences/MunkiReport"
PREFLIGHT=1
PREF_CMDS=( ) # Pref commands array
CURL="/usr/bin/curl --insecure --fail --silent  --show-error"

mkdir -p ./tmpcontents/usr/local/munki/{preflight.d,postflight_abort.d,postflight.d,munkilib}

#DON'T TOUCH
#Clone Munkireport
git clone https://github.com/munkireport/munkireport-php.git /tmp/munkireportlatest

#Reconfigure Simian
#GET MOST RECENT SIMIAN Preflight/Postflight/Broken SCRIPTS
${CURL} -L https://raw.githubusercontent.com/google/simian/master/src/simian/munki/postflight -o ./tmpcontents/usr/local/munki/preflight.d/0_simian
${CURL} -L https://raw.githubusercontent.com/google/simian/master/src/simian/munki/postflight -o ./tmpcontents/usr/local/munki/postflight.d/0_simian
${CURL} -L https://raw.githubusercontent.com/google/simian/master/src/simian/munki/report_broken_client -o ./tmpcontents/usr/local/munki/report_broken_client
sudo echo "
exec /usr/local/munki/munkireport_broken_client
" >> ./tmpcontents/usr/local/munki/report_broken_client


#Configure Munkireport
rm install_munkireport.sh
echo "#!/bin/bash
BASEURL="${BASEURL}"
MUNKIPATH="${MUNKIPATH}"
CACHEPATH="${MUNKIPATH}preflight.d/cache/"
" >> install_munkireport.sh

for file in $(find . -name 'munkireport-header.sh'); do cat $file >> install_munkireport.sh; echo " " >> install_munkireport.sh; done

for file in $(find /tmp/munkireportlatest/app/modules/*/scripts/ -name 'install.sh'); do cat $file >> install_munkireport.sh; echo " " >> install_munkireport.sh; done

for file in $(find . -name 'munkireport-footer.sh'); do cat $file >> install_munkireport.sh; echo " " >> install_munkireport.sh; done

mv /tmp/munkireportlatest/assets/client_installer/preflight ./tmpcontents/usr/local/munki/
mv /tmp/munkireportlatest/assets/client_installer/postflight ./tmpcontents/usr/local/munki/
mv /tmp/munkireportlatest/assets/client_installer/report_broken_client ./tmpcontents/usr/local/munki/munkireport_broken_client
mv /tmp/munkireportlatest/assets/client_installer/submit.preflight ./tmpcontents/usr/local/munki/preflight.d/


mv /tmp/munkireportlatest/assets/client_installer/phpserialize ./tmpcontents/usr/local/munki/munkilib/phpserialize.py
mv /tmp/munkireportlatest/assets/client_installer/reportcommon ./tmpcontents/usr/local/munki/munkilib/reportcommon.py
rm -Rf /tmp/munkireportlatest


#Set Permissions

sudo chown -R root:wheel tmpcontents/
sudo chmod -R 755 tmpcontents/