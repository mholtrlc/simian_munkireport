#!/bin/bash
BASEURL=http://report.reallifechurch.org/
MUNKIPATH=/usr/local/munki/
CACHEPATH=/usr/local/munki/preflight.d/cache/

CURL=/usr/bin/curl --insecure --fail --silent  --show-error

# Set munkireport preference
function setpref {
        PREF_CMDS=( "${PREF_CMDS[@]}" "defaults write ${PREFPATH} ${1} \"${2}\"" )
}

# Set munkireport reportitem preference
function setreportpref {
        setpref "ReportItems -dict-add ${1}" "${2}"
}

# Reset reportitems
function resetreportpref {
        PREF_CMDS=( "${PREF_CMDS[@]}" "defaults write ${PREFPATH} ReportItems -dict" )
}

 
#!/bin/bash

MODULE_NAME="ard"
MODULESCRIPT="init_ard"
PREF_FILE="/Library/Preferences/com.apple.RemoteDesktop.plist"

CTL="${BASEURL}index.php?/module/${MODULE_NAME}/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/${MODULESCRIPT}" -o "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Set preference to include this file in the preflight check
	setreportpref $MODULE_NAME "${PREF_FILE}"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Signal that we had an error
	ERR=1
fi 
#!/bin/bash

# bluetooth controller
CTL="${BASEURL}index.php?/module/bluetooth/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/bluetooth.sh" -o "${MUNKIPATH}preflight.d/bluetooth.sh"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/bluetooth.sh"

	# Set preference to include this file in the preflight check
	setreportpref "bluetooth" "${CACHEPATH}bluetoothinfo.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/bluetooth.sh"

	# Signal that we had an error
	ERR=1
fi


 
#!/bin/bash

MODULE_NAME="certificate"
MODULESCRIPT="cert_check"
PREF_FILE="${CACHEPATH}certificate.txt"

CTL="${BASEURL}index.php?/module/${MODULE_NAME}/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/${MODULESCRIPT}" -o "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Set preference to include this file in the preflight check
	setreportpref $MODULE_NAME "${PREF_FILE}"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Signal that we had an error
	ERR=1
fi
 
#!/bin/bash

# directory service controller
CTL="${BASEURL}index.php?/module/directory_service/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/directoryservice.sh" -o "${MUNKIPATH}preflight.d/directoryservice.sh"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/directoryservice.sh"

	# Set preference to include this file in the preflight check
	setreportpref "directory_service" "${CACHEPATH}directoryservice.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/directoryservice.sh"

	# Signal that we had an error
	ERR=1
fi


 
#!/bin/bash

# disk_report controller
DR_CTL="${BASEURL}index.php?/module/disk_report/"

# Get the scripts in the proper directories
${CURL} "${DR_CTL}get_script/disk_info" -o "${MUNKIPATH}preflight.d/disk_info"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/disk_info"

	# Set preference to include this file in the preflight check
	setreportpref "disk_report" "${CACHEPATH}disk.plist"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/disk_info"
	ERR=1
fi


 
#!/bin/bash

# directory service controller
CTL="${BASEURL}index.php?/module/displays_info/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/displays.py" -o "${MUNKIPATH}preflight.d/displays.py"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/displays.py"

	# Set preference to include this file in the preflight check
	setreportpref "displays_info" "${CACHEPATH}displays.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/displays.py"

	# Signal that we had an error
	ERR=1
fi
 
#!/bin/bash

# filevault_status_controller
FV_CTL="${BASEURL}index.php?/module/filevault_status/"

# Create /usr/local/bin if missing
mkdir -p "${INSTALLROOT}/usr/local/bin" || ERR=1

# Get the scripts in the proper directories
${CURL}  "${FV_CTL}get_script/filevault_2_status_check.sh" -o "${INSTALLROOT}/usr/local/bin/filevault_2_status_check.sh" \
	&& ${CURL}  "${FV_CTL}get_script/filevaultstatus" -o "${MUNKIPATH}preflight.d/filevaultstatus" \

# Check exit status of curl
if [ $? = 0 ]; then
	# Make scripts executable
	chmod a+x "${INSTALLROOT}/usr/local/bin/filevault_2_status_check.sh" "${MUNKIPATH}preflight.d/filevaultstatus"

	# Set preference to include this file in the preflight check
	setreportpref "filevault_status" "${CACHEPATH}filevaultstatus.txt"
else
	echo "! Failed to install all required components"
	echo "! Skipping filevault status report"
	rm -f "${INSTALLROOT}/usr/local/bin/filevault_2_status_check.sh" \
		"${MUNKIPATH}preflight.d/filevaultstatus"
	# Set the exit status
	ERR=1
fi

 
#!/bin/bash

# Add InstallHistory (in 10.5 you need SWU.log)

# Get major OS version (uses uname -r and bash substitution)
# osvers is 10 for 10.6, 11 for 10.7, 12 for 10.8, etc.
osversionlong=$(uname -r)
osvers=${osversionlong/.*/}

if (( $osvers < 10 )); then 
	IPATH="/Library/Logs/Software Update.log"
else
	IPATH="/Library/Receipts/InstallHistory.plist"
fi

setreportpref "installhistory" "${IPATH}"
 
#!/bin/bash

# inventory controller
DR_CTL="${BASEURL}index.php?/module/inventory/"

# Get the scripts in the proper directories
${CURL} "${DR_CTL}get_script/inventory_add_plugins" -o "${MUNKIPATH}postflight.d/inventory_add_plugins.py"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}postflight.d/inventory_add_plugins.py"

	# Set preference to include this file in the preflight check
	setreportpref "inventory" "/Library/Managed Installs/ApplicationInventory.plist"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}postflight.d/inventory_add_plugins.py"
	ERR=1
fi

 
#!/bin/bash

# localadmin controller
CTL="${BASEURL}index.php?/module/localadmin/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/localadmin" -o "${MUNKIPATH}preflight.d/localadmin"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/localadmin"

	# Set preference to include this file in the preflight check
	setreportpref "localadmin" "${CACHEPATH}localadmins.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/localadmin"

	# Signal that we had an error
	ERR=1
fi


 
#!/bin/bash

setreportpref "munkireport" '/Library/Managed Installs/ManagedInstallReport.plist'
 
#!/bin/bash

# filevault_status_controller
NW_CTL="${BASEURL}index.php?/module/network/"

# Get the script in the proper directory
${CURL} "${NW_CTL}get_script/networkinfo.sh" -o "${MUNKIPATH}preflight.d/networkinfo.sh"

if [ "${?}" != 0 ]
then
	echo "Failed to download all required components!"
	rm -f ${MUNKIPATH}preflight.d/networkinfo.sh
	exit 1
fi

# Make executable
chmod a+x "${MUNKIPATH}preflight.d/networkinfo.sh"

# Set preference to include this file in the preflight check
setreportpref "network" "${CACHEPATH}networkinfo.txt"
 
#!/bin/bash

# power controller
CTL="${BASEURL}index.php?/module/power/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/power.sh" -o "${MUNKIPATH}preflight.d/power.sh"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/power.sh"

	# Set preference to include this file in the preflight check
	setreportpref "power" "${CACHEPATH}powerinfo.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/power.sh"

	# Signal that we had an error
	ERR=1
fi
 
#!/bin/bash

# profile controller
CTL="${BASEURL}index.php?/module/profile/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/profile.py" -o "${MUNKIPATH}preflight.d/profile.py"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/profile.py"

	# Set preference to include this file in the preflight check
	setreportpref "profile" "${CACHEPATH}profile.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/profile.py"

	# Signal that we had an error
	ERR=1
fi
 
#!/bin/bash

# Create munkireportlib directory
mkdir -p ${MUNKIPATH}munkireportlib

# servermetrics controller
CTL="${BASEURL}index.php?/module/servermetrics/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/servermetrics.py" -o "${MUNKIPATH}preflight.d/servermetrics.py" && 
${CURL} "${CTL}get_script/ccl_asldb.py" -o "${MUNKIPATH}munkireportlib/ccl_asldb.py"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/servermetrics.py"

	# Set preference to include this file in the preflight check
	setreportpref "servermetrics" "${CACHEPATH}servermetrics.json"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/servermetrics.py"

	# Signal that we had an error
	ERR=1
fi
 
#!/bin/bash

MODULE_NAME="service"
MODULESCRIPT="service_check"
MODULE_CACHE_FILE="services.txt"

CTL="${BASEURL}index.php?/module/${MODULE_NAME}/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/${MODULESCRIPT}" -o "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Set preference to include this file in the preflight check
	setreportpref $MODULE_NAME "${CACHEPATH}${MODULE_CACHE_FILE}"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Signal that we had an error
	ERR=1
fi
 
#!/bin/bash

# servermetrics controller
CTL="${BASEURL}index.php?/module/timemachine/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/timemachine.sh" -o "${MUNKIPATH}preflight.d/timemachine.sh"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/timemachine.sh"

	# Set preference to include this file in the preflight check
	setreportpref "timemachine" "${CACHEPATH}timemachine.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/timemachine.sh"

	# Signal that we had an error
	ERR=1
fi
 
#!/bin/bash

# warranty controller
CTL="${BASEURL}index.php?/module/warranty/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/warranty" -o "${MUNKIPATH}preflight.d/warranty"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/warranty"

	# Set preference to include this file in the preflight check
	setreportpref "warranty" "${CACHEPATH}warranty.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/warranty"

	# Signal that we had an error
	ERR=1
fi


 

setpref BaseURL "${BASEURL}"


 
