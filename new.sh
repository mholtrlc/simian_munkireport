#!/bin/bash
BASEURL=https://munkireport.reallifechurch.org/
MODULES="ard,bluetooth,certificate,directory_service,disk_report,displays_info,filevault_status,inventory,localadmin,munkireport,network,power,profile,service,timemachine,warranty"
BASEURL="https://munkireport.reallifechurch.org/"
TPL_BASE="${BASEURL}/assets/client_installer/"
MUNKIPATH="/usr/local/munki/" # TODO read munkipath from munki config
CACHEPATH="${MUNKIPATH}preflight.d/cache/"
PREFPATH="/Library/Preferences/MunkiReport"
PREFLIGHT=1
PREF_CMDS=( ) # Pref commands array
CURL=/usr/bin/curl
# Exit status
ERR=0

# Packaging
BUILDPKG=0
IDENTIFIER="com.github.munkireport"

VERSION="2.5.0"

rm install_modules.sh

cat header.sh >> install_modules.sh

IFS=","
for modulename in $MODULES
do
	echo "---------------------------"
	echo "Getting Module: $modulename"
	echo "---------------------------"
	echo "
	" >> install_modules.sh
	$CURL ${BASEURL}index.php?/module/$modulename/get_script/install.sh >> install_modules.sh
	echo "
	" >> install_modules.sh
	echo "Success"
	echo ""
	echo ""
done


cat footer.sh >> install_modules.sh