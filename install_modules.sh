#!/bin/bash

BASEURL="https://munkireport.reallifechurch.org/"
TPL_BASE="${BASEURL}/assets/client_installer/"
MUNKIPATH="/usr/local/munki/" # TODO read munkipath from munki config
CACHEPATH="${MUNKIPATH}preflight.d/cache/"
PREFPATH="/Library/Preferences/MunkiReport"
PREFLIGHT=1
PREF_CMDS=( ) # Pref commands array
CURL="/usr/bin/curl --insecure --fail --silent  --show-error"
# Exit status
ERR=0

# Packaging
BUILDPKG=0
IDENTIFIER="com.github.munkireport"

VERSION="2.5.0.1"

function usage {
	PROG=$(basename $0)
	cat <<EOF >&2
Usage: ${PROG} [OPTIONS]

  -b URL    Base url to munki report server
            Current value: ${BASEURL}
  -m PATH   Path to installation directory
            Current value: ${MUNKIPATH}
  -p PATH   Path to preferences file (without the .plist extension)
            Current value: ${PREFPATH}
  -n        Do not run preflight script after the installation
  -i PATH   Create a full installer at PATH
  -c ID     Change pkg id to ID
  -h        Display this help message
  -v VERS   Override version number

Example:
  * Install munkireport client scripts into the default location and run the
    preflight script.

        $PROG

  * Install munkireport and preferences into a custom location ready to be
    packaged.

        $PROG -b ${BASEURL} \\
              -m ~/Desktop/munkireport-$VERSION/usr/local/munki/ \\
              -p ~/Desktop/munkireport-$VERSION/Library/Preferences/MunkiReport \\
              -n

  * Create a package installer for munkireport.

        $PROG -i ~/Desktop
EOF
}

# Set munkireport preference
function setpref {
	PREF_CMDS=( "${PREF_CMDS[@]}" "defaults write ${PREFPATH} ${1} \"${2}\"" )
}

# Set munkireport reportitem preference
function setreportpref {
	setpref "ReportItems -dict-add ${1}" "${2}"
}

# Reset reportitems
function resetreportpref {
	PREF_CMDS=( "${PREF_CMDS[@]}" "defaults write ${PREFPATH} ReportItems -dict" )
}

while getopts b:m:p:c:v:i:nh flag; do
	case $flag in
		b)
			BASEURL="$OPTARG"
			;;
		m)
			MUNKIPATH="$OPTARG"
			;;
		p)
			PREFPATH="$OPTARG"
			;;
		c)
			IDENTIFIER="$OPTARG"
			;;
		v)
			VERSION="$OPTARG"
			;;
		i)
			PKGDEST="$OPTARG"
			# Create temp directory
			INSTALLTEMP=$(mktemp -d -t mrpkg)
			INSTALLROOT="$INSTALLTEMP"/install_root
			MUNKIPATH="$INSTALLROOT"/usr/local/munki/
			PREFPATH=/Library/Preferences/MunkiReport
			PREFLIGHT=0
			BUILDPKG=1
			;;
		n)
			PREFLIGHT=0
			;;
		h|?)
			usage
			exit
			;;
	esac
done

echo "Preparing ${MUNKIPATH} and ${PREFPATH}"
mkdir -p "$(dirname ${PREFPATH})"
mkdir -p "${MUNKIPATH}munkilib"

echo "BaseURL is ${BASEURL}"

echo "Retrieving munkireport scripts"

cd ${MUNKIPATH}
$CURL "${TPL_BASE}{preflight,postflight}" --remote-name --remote-name  \
	&& $CURL "${TPL_BASE}report_broken_client" -o "${MUNKIPATH}report_broken_client_munkireport" \
	&& $CURL "${TPL_BASE}reportcommon" -o "${MUNKIPATH}munkilib/reportcommon.py" \
	&& $CURL "${TPL_BASE}phpserialize" -o "${MUNKIPATH}munkilib/phpserialize.py" \
	&& $CURL "https://raw.githubusercontent.com/google/simian/master/src/simian/munki/postflight" -o "${MUNKIPATH}preflight.d/0_simian" #\
	#&& $CURL "https://raw.githubusercontent.com/google/simian/master/src/simian/munki/postflight" -o "${MUNKIPATH}postflight.d/0_simian" \
	#&& $CURL "https://raw.githubusercontent.com/google/simian/master/src/simian/munki/report_broken_client" -o "{MUNKIPATH}report_broken_client" 
echo "Got the first bit"

if [ "${?}" != 0 ]
then
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}"{preflight,postflight,report_broken_client} \
		"${MUNKIPATH}"munkilib/reportcommon.py
	exit 1
fi

chmod a+x "${MUNKIPATH}"{preflight,postflight,report_broken_client}

# Create preflight.d + download scripts
mkdir -p "${MUNKIPATH}preflight.d"
cd "${MUNKIPATH}preflight.d"
${CURL} "${TPL_BASE}submit.preflight" --remote-name

if [ "${?}" != 0 ]
then
	echo "Failed to download preflight script!"
	rm -f "${MUNKIPATH}preflight.d/submit.preflight"
else
	chmod a+x "${MUNKIPATH}preflight.d/submit.preflight"
fi

# Create postflight.d
mkdir -p "${MUNKIPATH}postflight.d"

# Create preflight_abort.d
mkdir -p "${MUNKIPATH}preflight_abort.d"

echo "Configuring munkireport"
#### Configure Munkireport ####

# Set BaseUrl preference
setpref 'BaseUrl' "${BASEURL}"

# Reset ReportItems array
resetreportpref

# Include module scripts

## munkireport ## 
echo '+ Installing munkireport'


	
#!/bin/bash

MODULE_NAME="ard"
MODULESCRIPT="init_ard"
PREF_FILE="/Library/Preferences/com.apple.RemoteDesktop.plist"

CTL="${BASEURL}index.php?/module/${MODULE_NAME}/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/${MODULESCRIPT}" -o "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Set preference to include this file in the preflight check
	setreportpref $MODULE_NAME "${PREF_FILE}"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Signal that we had an error
	ERR=1
fi
	

	
#!/bin/bash

# bluetooth controller
CTL="${BASEURL}index.php?/module/bluetooth/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/bluetooth.sh" -o "${MUNKIPATH}preflight.d/bluetooth.sh"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/bluetooth.sh"

	# Set preference to include this file in the preflight check
	setreportpref "bluetooth" "${CACHEPATH}bluetoothinfo.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/bluetooth.sh"

	# Signal that we had an error
	ERR=1
fi



	

	
#!/bin/bash

MODULE_NAME="certificate"
MODULESCRIPT="cert_check"
PREF_FILE="${CACHEPATH}certificate.txt"

CTL="${BASEURL}index.php?/module/${MODULE_NAME}/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/${MODULESCRIPT}" -o "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Set preference to include this file in the preflight check
	setreportpref $MODULE_NAME "${PREF_FILE}"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Signal that we had an error
	ERR=1
fi

	

	
#!/bin/bash

# directory service controller
CTL="${BASEURL}index.php?/module/directory_service/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/directoryservice.sh" -o "${MUNKIPATH}preflight.d/directoryservice.sh"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/directoryservice.sh"

	# Set preference to include this file in the preflight check
	setreportpref "directory_service" "${CACHEPATH}directoryservice.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/directoryservice.sh"

	# Signal that we had an error
	ERR=1
fi



	

	
#!/bin/bash

# disk_report controller
DR_CTL="${BASEURL}index.php?/module/disk_report/"

# Get the scripts in the proper directories
${CURL} "${DR_CTL}get_script/disk_info" -o "${MUNKIPATH}preflight.d/disk_info"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/disk_info"

	# Set preference to include this file in the preflight check
	setreportpref "disk_report" "${CACHEPATH}disk.plist"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/disk_info"
	ERR=1
fi



	

	
#!/bin/bash

# directory service controller
CTL="${BASEURL}index.php?/module/displays_info/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/displays.py" -o "${MUNKIPATH}preflight.d/displays.py"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/displays.py"

	# Set preference to include this file in the preflight check
	setreportpref "displays_info" "${CACHEPATH}displays.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/displays.py"

	# Signal that we had an error
	ERR=1
fi

	

	
#!/bin/bash

# filevault_status_controller
FV_CTL="${BASEURL}index.php?/module/filevault_status/"

# Create /usr/local/bin if missing
mkdir -p "${INSTALLROOT}/usr/local/bin" || ERR=1

# Get the scripts in the proper directories
${CURL}  "${FV_CTL}get_script/filevault_2_status_check.sh" -o "${INSTALLROOT}/usr/local/bin/filevault_2_status_check.sh" \
	&& ${CURL}  "${FV_CTL}get_script/filevaultstatus" -o "${MUNKIPATH}preflight.d/filevaultstatus" \

# Check exit status of curl
if [ $? = 0 ]; then
	# Make scripts executable
	chmod a+x "${INSTALLROOT}/usr/local/bin/filevault_2_status_check.sh" "${MUNKIPATH}preflight.d/filevaultstatus"

	# Set preference to include this file in the preflight check
	setreportpref "filevault_status" "${CACHEPATH}filevaultstatus.txt"
else
	echo "! Failed to install all required components"
	echo "! Skipping filevault status report"
	rm -f "${INSTALLROOT}/usr/local/bin/filevault_2_status_check.sh" \
		"${MUNKIPATH}preflight.d/filevaultstatus"
	# Set the exit status
	ERR=1
fi


	

	
#!/bin/bash

# inventory controller
DR_CTL="${BASEURL}index.php?/module/inventory/"

# Get the scripts in the proper directories
${CURL} "${DR_CTL}get_script/inventory_add_plugins" -o "${MUNKIPATH}postflight.d/inventory_add_plugins.py"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}postflight.d/inventory_add_plugins.py"

	# Set preference to include this file in the preflight check
	setreportpref "inventory" "/Library/Managed Installs/ApplicationInventory.plist"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}postflight.d/inventory_add_plugins.py"
	ERR=1
fi


	

	
#!/bin/bash

# localadmin controller
CTL="${BASEURL}index.php?/module/localadmin/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/localadmin" -o "${MUNKIPATH}preflight.d/localadmin"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/localadmin"

	# Set preference to include this file in the preflight check
	setreportpref "localadmin" "${CACHEPATH}localadmins.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/localadmin"

	# Signal that we had an error
	ERR=1
fi



	

	
#!/bin/bash

setreportpref "munkireport" '/Library/Managed Installs/ManagedInstallReport.plist'

	

	
#!/bin/bash

# filevault_status_controller
NW_CTL="${BASEURL}index.php?/module/network/"

# Get the script in the proper directory
${CURL} "${NW_CTL}get_script/networkinfo.sh" -o "${MUNKIPATH}preflight.d/networkinfo.sh"

if [ "${?}" != 0 ]
then
	echo "Failed to download all required components!"
	rm -f ${MUNKIPATH}preflight.d/networkinfo.sh
	exit 1
fi

# Make executable
chmod a+x "${MUNKIPATH}preflight.d/networkinfo.sh"

# Set preference to include this file in the preflight check
setreportpref "network" "${CACHEPATH}networkinfo.txt"

	

	
#!/bin/bash

# power controller
CTL="${BASEURL}index.php?/module/power/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/power.sh" -o "${MUNKIPATH}preflight.d/power.sh"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/power.sh"

	# Set preference to include this file in the preflight check
	setreportpref "power" "${CACHEPATH}powerinfo.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/power.sh"

	# Signal that we had an error
	ERR=1
fi

	

	
#!/bin/bash

# profile controller
CTL="${BASEURL}index.php?/module/profile/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/profile.py" -o "${MUNKIPATH}preflight.d/profile.py"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/profile.py"

	# Set preference to include this file in the preflight check
	setreportpref "profile" "${CACHEPATH}profile.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/profile.py"

	# Signal that we had an error
	ERR=1
fi

	

	
#!/bin/bash

MODULE_NAME="service"
MODULESCRIPT="service_check"
MODULE_CACHE_FILE="services.txt"

CTL="${BASEURL}index.php?/module/${MODULE_NAME}/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/${MODULESCRIPT}" -o "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Set preference to include this file in the preflight check
	setreportpref $MODULE_NAME "${CACHEPATH}${MODULE_CACHE_FILE}"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/${MODULESCRIPT}"

	# Signal that we had an error
	ERR=1
fi

	

	
#!/bin/bash

# servermetrics controller
CTL="${BASEURL}index.php?/module/timemachine/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/timemachine.sh" -o "${MUNKIPATH}preflight.d/timemachine.sh"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/timemachine.sh"

	# Set preference to include this file in the preflight check
	setreportpref "timemachine" "${CACHEPATH}timemachine.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/timemachine.sh"

	# Signal that we had an error
	ERR=1
fi

	

	
#!/bin/bash

# warranty controller
CTL="${BASEURL}index.php?/module/warranty/"

# Get the scripts in the proper directories
${CURL} "${CTL}get_script/warranty" -o "${MUNKIPATH}preflight.d/warranty"

# Check exit status of curl
if [ $? = 0 ]; then
	# Make executable
	chmod a+x "${MUNKIPATH}preflight.d/warranty"

	# Set preference to include this file in the preflight check
	setreportpref "warranty" "${CACHEPATH}warranty.txt"

else
	echo "Failed to download all required components!"
	rm -f "${MUNKIPATH}preflight.d/warranty"

	# Signal that we had an error
	ERR=1
fi



	
# Store munkipath when building a package
if [ $BUILDPKG = 1 ]; then
	STOREPATH=${MUNKIPATH}
	MUNKIPATH='/usr/local/munki/'
fi

# Capture uninstall scripts
read -r -d '' UNINSTALLS << EOF


## ard ## 
echo '- Uninstalling ard'

#!/bin/bash

rm -f "${MUNKIPATH}preflight.d/init_ard"


## bluetooth ## 
echo '- Uninstalling bluetooth'

#!/bin/bash

# Remove bluetooth script
rm -f "${MUNKIPATH}preflight.d/bluetooth.sh"

# Remove bluetoothinfo.txt file
rm -f "${CACHEPATH}bluetoothinfo.txt"


## certificate ## 
echo '- Uninstalling certificate'

#!/bin/bash

MODULE_NAME="certificate"
MODULESCRIPT="cert_check"
MODULE_CACHE_FILE="certificate.txt"

# Remove preflight script
rm -f "${MUNKIPATH}preflight.d/cert_check"

# Remove cache file
rm -f "${CACHEPATH}certificate.txt"


## directory_service ## 
echo '- Uninstalling directory_service'

#!/bin/bash

# Remove directoryservice script
rm -f "${MUNKIPATH}preflight.d/directoryservice.sh"

# Remove directoryservice.txt
rm -f "${CACHEPATH}directoryservice.txt"



## disk_report ## 
echo '- Uninstalling disk_report'

#!/bin/bash

# Remove disk_info script
rm -f "${MUNKIPATH}preflight.d/disk_info"

# Remove disk.plist
rm -f "${CACHEPATH}disk.plist"



## displays_info ## 
echo '- Uninstalling displays_info'

#!/bin/bash

# Remove directoryservice script
rm -f "${MUNKIPATH}preflight.d/displays.py"

# Remove directoryservice.txt
rm -f "${CACHEPATH}displays.txt"


## filevault_status ## 
echo '- Uninstalling filevault_status'

#!/bin/bash

# Remove filevaultstatus script
rm -f "${MUNKIPATH}preflight.d/filevaultstatus"

# Remove the filevaultstatus.txt cache file
rm -f "${CACHEPATH}filevaultstatus.txt"

# We'll leave the check script alone
# rm -f /usr/local/bin/filevault_2_status_check.sh


## localadmin ## 
echo '- Uninstalling localadmin'

#!/bin/bash

# Remove localadmin script
rm -f "${MUNKIPATH}preflight.d/localadmin"

# Remove localadmins.txt file
rm -f "${CACHEPATH}localadmins.txt"


## network ## 
echo '- Uninstalling network'

#!/bin/bash

# Remove networkinfo script
rm -f "${MUNKIPATH}preflight.d/networkinfo.sh"

# Remove networkinfo.txt
rm -f "${CACHEPATH}networkinfo.txt"



## power ## 
echo '- Uninstalling power'

#!/bin/bash

# Remove power script
rm -f "${MUNKIPATH}preflight.d/power.sh"

# Remove powerinfo.txt file
rm -f "${MUNKIPATH}preflight.d/cache/powerinfo.txt"


## profile ## 
echo '- Uninstalling profile'

#!/bin/bash

# Remove power script
rm -f "${MUNKIPATH}preflight.d/profile.py"

# Remove powerinfo.txt file
rm -f "${MUNKIPATH}preflight.d/cache/profile.txt"


## servermetrics ## 
echo '- Uninstalling servermetrics'

#!/bin/bash

# Remove servermetrics script
rm -f "${MUNKIPATH}preflight.d/servermetrics.py"

# Remove servermetrics.json file
rm -f "${MUNKIPATH}preflight.d/cache/servermetrics.json"

# Keep ccl_asldb.py as it may be used by other scripts in the future
# rm -f ${MUNKIPATH}munkireportlib/ccl_asldb.py

## service ## 
echo '- Uninstalling service'

#!/bin/bash

rm -f "${MUNKIPATH}preflight.d/service_check"
rm -f "${CACHEPATH}services.txt"



## timemachine ## 
echo '- Uninstalling timemachine'

#!/bin/bash

# Remove timemachine script
rm -f "${MUNKIPATH}preflight.d/timemachine.sh"

# Remove timemachine.txt file
rm -f "${CACHEPATH}timemachine.txt"


## warranty ## 
echo '- Uninstalling warranty'

#!/bin/bash

# Remove warranty script
rm -f "${MUNKIPATH}preflight.d/warranty"

# Remove warranty.txt file
rm -f "${CACHEPATH}warranty.txt"


EOF

# Restore munkipath when building a package
if [ $BUILDPKG = 1 ]; then
	MUNKIPATH=${STOREPATH}
fi


# If not building a package, execute uninstall scripts
if [ $BUILDPKG = 0 ]; then
	eval "$UNINSTALLS"
	# Remove munkireport version file
	rm -f "${MUNKIPATH}munkireport-"*
fi

if [ $ERR = 0 ]; then

	if [ $BUILDPKG = 1 ]; then
		
		# Create scripts directory
		SCRIPTDIR="$INSTALLTEMP"/scripts
		mkdir -p "$SCRIPTDIR"
		
		# Add uninstall script to preinstall
		echo  "#!/bin/bash" > $SCRIPTDIR/preinstall
		echo  "$UNINSTALLS" >> $SCRIPTDIR/preinstall
		chmod +x $SCRIPTDIR/preinstall

		# Add Preference setting commands to postinstall
		echo  "#!/bin/bash" > $SCRIPTDIR/postinstall
		for i in "${PREF_CMDS[@]}";
			do echo $i >> $SCRIPTDIR/postinstall
		done
		chmod +x $SCRIPTDIR/postinstall


		echo "Building MunkiReport v${VERSION} package."
		pkgbuild --identifier "$IDENTIFIER" \
				 --version "$VERSION" \
				 --root "$INSTALLROOT" \
				 --scripts "$SCRIPTDIR" \
				 "$PKGDEST/munkireport-${VERSION}.pkg"

	else

		# Set preferences
		echo "Setting preferences"
		for i in "${PREF_CMDS[@]}"; do 
			eval $i
		done

		# Set munkireport version file
		touch "${MUNKIPATH}munkireport-${VERSION}"

		echo "Installation of MunkiReport v${VERSION} complete."
		echo 'Running the preflight script for initialization'
		if [ $PREFLIGHT = 1 ]; then
			${MUNKIPATH}preflight
		fi

	fi
	
else
	echo "! Installation of MunkiReport v${VERSION} incomplete."
fi

if [ "$INSTALLTEMP" != "" ]; then
	echo "Cleaning up temporary directory $INSTALLTEMP"
	rm -r $INSTALLTEMP
fi



exit $ERR